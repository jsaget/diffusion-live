'use strict';

var Promise = require('bluebird');
var fs = require('fs');
var _ = require('lodash');
var Hapi = require('hapi');
// var glob = Promise.promisify(require('glob'));
var Path = require('path');

var server = new Hapi.Server({
    cache: [{
      name: 'memCache',
      engine: require('catbox-memory'),
      partition: 'cache',
    // }
    // , {
    // name: 'redisCache',
    // engine: require('catbox-redis'),
    // host: '127.0.0.1',
    // partition: 'cache',
    // //password: '',
  }],
});

server.connection({ port: 3000 });
Promise.promisifyAll(server);

var currentSlide = [];

// Easy acces to cache
server.app.cache = server.cache({cache: 'memCache', segment: 'session'});

var io = require("socket.io")(server.listener);

io.on("connection", function(socket) {
  console.log('server.js:26', 'New connection');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

// function registerPlugins() {
//   return Promise.try(function() {
//       return server.registerAsync(require('hapi-auth-cookie'))
//     })
//     .tap(function() {
//       console.log('server.js:37', 'GOOO');
//       server.auth.strategy('sessionAuth', 'cookie', true, {
//         password: 'password-should-be-32-characters',
//         cookie: 'sid',
//         redirectTo: false,
//         isSecure: false,
//         ttl: 1000 * 60 * 20, // 20min
//         validateFunc: function (request, session, callback) {
//           return callback(null, true, {});
//           var cache = request.app.cache;
//           cache.get(session.sid, function(err, cached) {
//             if (err) {
//               return callback(err, false);
//             }
//             if (!cached) {
//               return callback(null, false);
//             }
//             return callback(null, true, cached.account);
//           });
//         },
//       });
//       console.log('server.js:57', 'done');
//     })
//   ;
// }


// function login(request, reply) {
//   console.log('server.js:73', request.cookieAuth);
//   if (request.auth.isAuthenticated) {
//     return reply.redirect('/');
//   }

//   if (!request.payload.username || !request.payload.password
//     || request.payload.username !== 'cdfr' || request.payload.password !== 'P@$$W0rd#CdFR2016') {
//     reply({err: 'Invalid credentials'});
//   }

//   var sid = uuid.v4();
//   request.server.app.cache.set(sid, {user: {username: 'cdfr'}}, 0, function (err) {
//     if (err) {
//       return reply(err);
//     }

//     request.cookieAuth.set({ sid: sid });
//     reply({username: 'cdfr', sid: sid});
//   });
// }

// function me(request, reply) {
//   var session = request.auth.credentials;
//   var user = request.server.app.cache.get(session.sid);
//   reply(user);
// }

// function logout(request, reply) {
//   request.cookieAuth.clear();
//   return reply({disconnect: true});
// }

function routesRegistration() {
  // server.route({
  //   method: 'GET',
  //   path: '/api/auth/disconnect',
  //   config: {
  //     handler: logout
  //   }
  // });

  // server.route({
  //   method: 'POST',
  //   path: '/api/auth/authenticate',
  //   config: {
  //     handler: login,
  //     auth: { mode: 'try' },
  //     plugins: {
  //       'hapi-auth-cookie': {
  //         redirectTo: false
  //       }
  //     }
  //   },
  // });

  // server.route({
  //   method: 'POST',
  //   path: '/api/auth/_me',
  //   config: {
  //     handler: me,
  //     auth: { mode: 'try' },
  //     plugins: {
  //       'hapi-auth-cookie': {
  //         redirectTo: false
  //       }
  //     }
  //   },
  // });

  server.route({
    method: 'POST',
    path: '/api/slides/set-default-slide',
    handler: function (request, reply) {
      console.log('server.js:21', 'set default slide');
      var data = request.payload;
      console.log('server.js:149', data);
      io.emit('setDefaultSlide', data);
      reply('Hello!');
    }
  });

  server.route({
    method: 'POST',
    path: '/api/slides/set',
    handler: function (request, reply) {
      console.log('server.js:21', 'set slides');
      var data = request.payload;

      // save last pushed slides
      currentSlide = data;

      io.emit('setSlide', data);
      reply('Hello!');
    }
  });

  // server.route({
  //   method: 'GET',
  //   path: '/api/slides/add',
  //   handler: function (request, reply) {
  //     console.log('server.js:21', 'ADD slide');
  //     var data = {
  //       id: '5',
  //       html: '<span>yahhhhahahahhah</span>',
  //       duration: 3000,
  //     };
  //     io.emit('addSlide', data);
  //     reply('Hello!');
  //   }
  // });

  // server.route({
  //   method: 'GET',
  //   path: '/api/slides/remove',
  //   handler: function (request, reply) {
  //     console.log('server.js:21', 'remove slide');
  //     io.emit('removeSlide', 'aaaa');
  //     reply('Hello!');
  //   }
  // });

  server.route({
    method: 'GET',
    path: '/api/get-current-slide',
    handler: function (request, reply) {
      console.log('server.js:21', 'get-current-slide');
      io.emit('setSlide', currentSlide);
      reply(currentSlide);
    }
  });

  server.route({
    method: 'GET',
    path: '/api/slides/remove-all',
    handler: function (request, reply) {
      console.log('server.js:21', 'Remove All');
      io.emit('removeAllSlides', {});
      reply('Hello!');
    }
  });

  server.route({
    method: 'GET',
    path: '/api/slides/reload',
    handler: function (request, reply) {
      console.log('server.js:21', 'reload');
      io.emit('reload', {});
      reply('Hello!');
    }
  });

  server.route({
    method: 'POST',
    path: '/api/slides/force-display',
    handler: function (request, reply) {
      console.log('server.js:21', 'forceDisplay');
      var data = request.payload;
      io.emit('forceDisplay', data);
      reply('Hello!');
    }
  });


  server.route({
    method: 'GET',
    path: '/api/slides/start',
    handler: function (request, reply) {
      console.log('server.js:21', 'start');
      io.emit('start', {});
      reply('Hello!');
    }
  });

  server.route({
    method: 'GET',
    path: '/api/slides/restart',
    handler: function (request, reply) {
      console.log('server.js:21', 'restart Slide Show');
      io.emit('restartSlideShow', {});
      reply('Hello!');
    }
  });

  server.route({
    method: 'GET',
    path: '/api/slides/stop',
    handler: function (request, reply) {
      console.log('server.js:21', 'Stop');
      io.emit('stop', {});
      reply('Hello!');
    }
  });

  server.route({
    method: 'POST',
    path: '/api/slides/upload',
    config: {
      payload: {
        output: 'stream',
        parse: true, // true
        allow: 'multipart/form-data',
        maxBytes: 16000000,
      },
    },
    handler: function (request, reply) {
      console.log('Upload images');
      var data = request.payload;
      if (!data.file) {
        console.log('nothing to upload');
        return reply();
      }

      // console.log('server.js:268', data);
      if (Array.isArray(data.file)) {
        console.log('server.js:279', 'multi files');
        var res = [];
        _.each(data.file, function(dataFile) {
          var name = dataFile.hapi.filename;
          var path = process.cwd() + "/build/browser/statics/img/" + name;
          var file = fs.createWriteStream(path);

          file.on('error', function (err) {
            console.error('error while saving file', err);
          });

          dataFile.pipe(file);

          dataFile.on('end', function (err) {
            res.push({
              filename: dataFile.hapi.filename,
              headers: dataFile.hapi.headers
            });
          });
        });
        reply(JSON.stringify(res));

      } else {
        console.log('server.js:279', 'mono file upload');
        var name = data.file.hapi.filename;
        var path = process.cwd() + "/build/browser/statics/img/" + name;
        var file = fs.createWriteStream(path);

        file.on('error', function (err) {
          console.error('error while saving file', err)
        });

        data.file.pipe(file);

        data.file.on('end', function (err) {
          var ret = {
            filename: data.file.hapi.filename,
            headers: data.file.hapi.headers
          }
          reply(JSON.stringify(ret));
        });

      }
    }
  });

  // server.route({
  //   method: 'GET',
  //   path: '/api/add',
  //   handler: function (request, reply) {
  //     console.log('server.js:21', 'ADD ITEM');
  //     io.emit('addSlide', 'msg');
  //     reply('Hello!');
  //   }
  // });

  server.register(require('inert'), function(err) {

    if (err) {
      throw err;
    }

    server.route({
      config: {
        auth: false,
      },
      method: 'GET',
      path: '/thisisahiddenpageforadministration8631435887654221234567890987653223467890097876433',
      handler: function (request, reply) {
        console.log('server.js:27', 'admin');

        reply.file('./build/admin/index.html');
      }
    });

    server.route({
      config: {
        auth: false,
      },
      method: 'GET',
      path: '/browser',
      handler: function (request, reply) {
        console.log('server.js:35', 'BROWSER');
        reply.file('./build/browser/index.html');
      }
    });

  /*  server.route({
      method: 'GET',
      path: '/statics/{param*}',
      handler: {
        directory: {
          path: './client/browser/statics/',
          lookupCompressed: true,
        },
      },
    });
  */
    server.route({
      config: {
        auth: false,
        cache : false,
      },
      method: 'GET',
      path: '/client/{params*}',
      handler: {
        directory: {
          path: './build/',
          lookupCompressed: true,
        },
      },
    });

  });
}



function startServer() {
  server.start(function(err) {
      if (err) {
          throw err;
      }
      console.log('Server running at:', server.info.uri);
  });
}




// var ioHandler = function (socket) {
//   console.log('connected');

//   socket.emit("welcome", {
//       message: "Hello from Hapi!",
//       version: Hapi.version
//   })

//   // simple echo service
//   var pingHandler = function(data) {
//       socket.emit("pong", data)
//   }

//   socket.on("ping", pingHandler)
// }


/*registerPlugins()
  .then(function() {
    routesRegistration();
  })*/

Promise.try(function() {
    return routesRegistration();
  })
  .then(function() {
    startServer();
  })
;

