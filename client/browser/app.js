'use strict';

angular.module('myApp', ['ngSanitize'])
.controller('mainCtrl', function ($scope, $sce, $timeout, $http) {


    function buildSvgSlide(data) {
        return {
            id: data.id,
            html: '',
            js: function() {
                console.log('display svg: ', data.svg);
                document.body.style.background = 'url(client/browser/statics/img/' + data.svg + ') no-repeat center center fixed';
            },
            duration: data.duration * 1000 || 10000,
        };
    }

    var defaultSlide = {
        html: '',
        js: function() {
            document.body.style.background = "url(client/browser/statics/img/default.svg) no-repeat center center fixed";
        },
        duration: 10000,
    };

    var slides = [];

    var currentSetTimeout; // use to kill the loop
    var breakSlide;
    $scope.currentSlide = defaultSlide;
    if (defaultSlide.js)
        defaultSlide.js();

    function cleanDeprecatedSlides() {
        var now = Date.now();
        _.remove(slides, function(value) {
            return value.to < now;
        });
    }

    function getNextSlide() {
        var nextSlide;

        console.log('slides.length', slides.length);
        console.log('$scope.currentSlide', $scope.currentSlide);
        if (slides.length === 0) { // set default slide
            if ($scope.currentSlide && $scope.currentSlide.id) {
                nextSlide = defaultSlide;
            } else {
                // default slide already selected
                // do nothing
            }
        } else { // select next slide
            if (!$scope.currentSlide || !$scope.currentSlide.id) {
                nextSlide = slides[0];
            } else {
                var currentSlideId = $scope.currentSlide.id;
                var currentSlide = _.find(slides, function(slide) {
                    return currentSlideId === slide.id;
                });

                if (currentSlide) {
                    var currentIdx = slides.indexOf(currentSlide);
                    if (currentIdx >= 0) {
                        currentIdx = currentIdx + 1 < slides.length ? currentIdx + 1 : 0;
                        nextSlide = slides[currentIdx];
                    } else {
                        nextSlide = slides[0];
                    }
                } else {
                    nextSlide = slides[0];
            }
            }
        }

        return nextSlide;
    }

    function runSlideShow() {
        cleanDeprecatedSlides();

         var nextSlide = getNextSlide();
         console.log('NEXTSLIDE', nextSlide);
         if (nextSlide) {
            $scope.currentSlide = nextSlide;
            if (nextSlide.js) {
                console.log('app.js:83', 'JS !!!');
                $timeout(nextSlide.js);
            }
        }
        console.log('app.js:46', $scope.currentSlide);

        currentSetTimeout = $timeout(runSlideShow, $scope.currentSlide.duration);
    }

   runSlideShow();

    var socket;
    socket = io(); // io.connect("http://localhost:3000");

    socket.on("connect", function () {
        console.log('app.js:97', 'CONNECT To server');
        $http.get('/api/get-current-slide');
    });

    socket.on('reload', reload);
    socket.on("start", start);
    socket.on("stop", stop);
    socket.on("restartSlideShow", restartSlideShow);
    socket.on("setSlide", setSlide);
    socket.on("removeAllSlides", removeAllSlides);
    socket.on("setDefaultSlide", setDefaultSlide);
    socket.on("forceDisplay", forceDisplay);
    // socket.on("addSlide", addSlide);
    // socket.on("removeSlide", removeSlide);

    function reload() {
        console.log('app.js:170', 'Reload!!!');
        window.location.reload();
    }

    function start(data) {
        console.log('Start slide show');
        if (currentSetTimeout) {
            console.log('app.js:143',  'slide already running');
            return;
        }

        if (breakSlide) {
            $scope.currentSlide = breakSlide;
            currentSetTimeout = $timeout(runSlideShow, $scope.currentSlide.duration);
        } else {
            runSlideShow();
        }
    }

    function stop(data) {
        console.log('Stop slide show');
        $timeout.cancel(currentSetTimeout);
        currentSetTimeout = undefined;
        $scope.currentSlide = defaultSlide;
    }

    function restartSlideShow() {
        console.log('reset slide show');
        $scope.currentSlide = undefined;
    }

    function forceDisplay(data) {
        console.log('force display slide ', data);
        var slide = buildSvgSlide(data);

        $timeout.cancel(currentSetTimeout);
        currentSetTimeout = undefined;
        breakSlide = $scope.currentSlide;
        $scope.currentSlide = slide;
        if (slide.js)
            $timeout(slide.js);
    }

    function setDefaultSlide(data) {
        console.log('Set defualt Slide', data);
        defaultSlide = buildSvgSlide(data);
        if (defaultSlide.js)
            defaultSlide.js();
    }

    function setSlide(data) {
        console.log('Set Slides', data);
        _.each(data, function(slide) {
            slides.push(buildSvgSlide(slide));
        })
        console.log('New set of slides: ', slides);
    }


    // function addSlide(data) {
    //     console.log('Add slides', data);
    //     slides.push(data);
    //     console.log('New set of slides: ', slides);
    // }


    // function removeSlide(slideId) {
    //     console.log('REMOVE SLIDE', slideId);
    //     _.remove(slides, function(value) {
    //         return value.id === slideId;
    //     });
    //     console.log('New set of slides: ', slides);
    // }

    function removeAllSlides() {
        console.log('Remove all slide');
        $timeout.cancel(currentSetTimeout);
        currentSetTimeout = undefined;
        slides = [];
        $scope.currentSlide = defaultSlide;
        if (defaultSlide.js)
            $timeout(defaultSlide.js);
    }
});




// function slide(img, txt) {
//     return $sce.trustAsHtml('<div style="width: 100%; height: 100%; background-color: black; color: white; text-align: center;">\
//         <div style="width: 100%; height: 100%; position: absolute; top:0; bottom: 0; left: 0; right: 0;margin: auto;">\
//             <img style="max-width:1200px; max-height:830px;" src="./client/browser/statics/img/' + img +'"/>\
//             <div style="text-align: center;"><i>' + txt + '</i></div>\
//         </div>\
//     </div>');
// }




// var slides = [{
//         id: '-1',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0000.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     },{
//         id: '0',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0001.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     },{
//         id: '1',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0002.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '2',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0003.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '3',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0004.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '4',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0005.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '5',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0006.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '6',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0007.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '7',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0008.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '8',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0009.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '9',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0010.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '10',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0011.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '11',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0012.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '12',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0013.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '13',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0014.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '14',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0015.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '15',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0016.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '16',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0017.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '17',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0018.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '18',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0019.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '19',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0020.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }, {
//         id: '20',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0021.svg) no-repeat center center fixed";
//         },
//         duration: 20000,
//     }, {
//         id: '21',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0022.svg) no-repeat center center fixed";
//         },
//         duration: 20000,
//     }, {
//         id: '23',
//         html: '',
//         js: function() {
//             document.body.style.background = "url(client/browser/statics/img/0026.svg) no-repeat center center fixed";
//         },
//         duration: 10000,
//     }];
