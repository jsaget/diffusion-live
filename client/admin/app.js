'use strict';

angular.module('myApp', ['ngSanitize', 'dndLists'])
.directive('fileInput', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, elm, attrs) {
            elm.bind('change', function() {
                $parse(attrs.fileInput)
                .assign(scope, elm[0].files);
               scope.$apply();
            });
        },
    };
})
.controller('mycontroller', function ($scope, $http) {

    $scope.dndItemSelected = undefined;
    $scope.slides = [];

    $http.get('/api/get-current-slide')
        .then(function(resp) {
            console.log('app.js:23', resp);
            if (resp.data) {
                console.log('app.js:25', 'set prevuous slide', resp.data);
                $scope.slides = resp.data;
            }
        })
    ;

    $scope.addSlide = function(data) {
        $scope.slides.push({
            id: data.image,
            html: '',
            svg: data.image,
            // js: function() {
            //     document.body.style.background = 'url(client/browser/statics/img/' + data.image + ') no-repeat center center fixed';
            // },
            duration: data.duration,
        });
        $scope.addSlide = {};
    };

    $scope.editSlides = function() {
        $scope.bulkAddSlides = _.reduce($scope.slides, function(acc, obj) {
            var id = (obj.svg !== obj.id) ? ';' + obj.id : '';
            if (acc.length) {
                acc += '\n';
            }
            acc += obj.svg + ';' + obj.duration + id;

            return acc;
        }, '');
    }

    $scope.bulkSlides = function(data) {
        var lines = data.split('\n');
        _.each(lines, function(line) {
            var obj = line.split(';');
            var newSlide = {
                id: obj.length === 3 && obj[2] ? obj[2] : obj[0],
                html: '',
                svg: obj[0],
                // js: function() {
                //     document.body.style.background = 'url(client/browser/statics/img/' + data.image + ') no-repeat center center fixed';
                // },
                duration: obj[1],
            };
            $scope.slides.push(newSlide);
        });
        $scope.bulkAddSlides = undefined;
    };

    $scope.clearLocalSlide = function() {
        $scope.slides = [];
    };


    $scope.deleteLocalSlide = function(id) {
        _.remove($scope.slides, function(obj) {
            return obj.id === id;
        })
    };

    $scope.resetAddSlide = function() {
        $scope.addSlide = {};
    };

    $scope.resetDefaultSlide = function() {
        $scope.setDefaultSlide = {};
    };

    $scope.resetForceSlide = function() {
        $scope.forceSlide = {};
    };

    $scope.pushAllSlides = function() {
        $http.post('/api/slides/set', $scope.slides)
        .then(function(res) {
            console.log('Set Slide OK');
        })
        .catch(function(err) {
            console.log('Set Slide Error. ', err);
        });
    };

    $scope.clearRemoteSlides = function() {
        $http.get('/api/slides/remove-all')
        .then(function(res) {
            console.log('Remove All Slide OK');
        })
        .catch(function(err) {
            console.log('Remove All Slide Error. ', err);
        });
    };

    $scope.startSlideShow = function() {
        var url = '/api/slides/start';

        $http.get(url)
        .then(function(res) {
            console.log('Start OK');
        })
        .catch(function(err) {
            console.log('Start Error. ', err);
        });
    };


    $scope.stopSlideShow = function() {
        var url = '/api/slides/stop';

        $http.get(url)
        .then(function(res) {
            console.log('Stop OK');
        })
        .catch(function(err) {
            console.log('Stop Error. ', err);
        });
    };

    $scope.restartSlideShow = function() {
        var url = '/api/slides/restart';

        $http.get(url)
        .then(function(res) {
            console.log('Restart OK');
        })
        .catch(function(err) {
            console.log('Restart Error. ', err);
        });
    };

    $scope.setDefaultSlide = function(data) {
        var url = '/api/slides/set-default-slide';
        var defaultSlide = {
            id: data.image,
            html: '',
            svg: data.image,
            // js: function() {
            //     document.body.style.background = 'url(client/browser/statics/img/' + data.image + ') no-repeat center center fixed';
            // },
            duration: data.duration,
        };
        $scope.setDefaultSlide = {};

        $http.post(url, defaultSlide)
        .then(function(res) {
            console.log('Set Default Slide OK');
        })
        .catch(function(err) {
            console.log('Set Default Slide Error', err);
        });
    };

    $scope.forceDisplaySlide = function(data) {
        var url = '/api/slides/force-display';
        var forceSlide = {
            id: data.image,
            html: '',
            svg: data.image,
            // js: function() {
            //     document.body.style.background = 'url(client/browser/statics/img/' + data.image + ') no-repeat center center fixed';
            // },
            duration: data.duration,
        };
        $scope.forceSlide = {};

        $http.post(url, forceSlide)
        .then(function(res) {
            console.log('Force Display OK');
        })
        .catch(function(err) {
            console.log('Force Display Error. ', err);
        });
    };

    $scope.reload = function() {
        console.log('app.js:35', 'CALL RELOAD');
        $http.get('/api/slides/reload')
        .then(function(res) {
            console.log('Reload Browser OK');
        })
        .catch(function(err) {
            console.log('Reload Browser Error. ', err);
        });
    };

    $scope.upload = function() {
        console.log('app.js:189', $scope.uploadFo);
        console.log('app.js:319', 'upload data');
        var fd = new FormData();
        angular.forEach($scope.files, function(file) {
            fd.append('file', file);
        });
        // console.log('app.js:324', fd.getAll('file'));
        $http.post('/api/slides/upload', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(d) {
            console.log('Upload Image', d);
            $scope.files = undefined;
        })
        .catch(function(err) {
            console.log('Upload Image Error. ', err);
            $scope.files = undefined;
        })
    };
})

// .service('AuthService', function($http, $state) {
//   'ngInject';

//   var currentUser;

//   function setCurrentUser(user) {
//     var oldUser = currentUser;
//     currentUser = user;

//     if ($state.current.abstract || currentUser === oldUser ||
//       (currentUser && oldUser && currentUser._id === oldUser._id))
//       return;

//     return $state.reload();
//   }

//   this.getCurrentUser = function() {
//     return currentUser ||
//       $http.get('/api/auth/_me').get('data')
//         .catch(function() {
//           return null;
//         })
//         .tap(setCurrentUser)
//     ;
//   };

//   this.authenticate = function(credentials) {
//     return $http.post('/api/auth/authenticate', credentials)
//       .get('data')
//       .then(function(user) {
//         setCurrentUser(user);
//         return user;
//       })
//     ;
//   };

//   this.disconnect = function() {
//     return $http.get('/api/auth/disconnect')
//       .then(setCurrentUser(null))
//     ;
//   };
// })

// .config(function($stateProvider, $urlRouterProvider) {
//   'ngInject';

//   $stateProvider
//     .state('app', {
//       abstract: true,
//       url: '',
//       template: '<div ui-view></div>',
//       resolve: {
//         user: function(AuthService) {
//           return AuthService.getCurrentUser();
//         },
//         currentLng: function(localStorageService) {
//           return localStorageService.get('currentLng') || {};
//         },
//       },
//       controller: indexController,
//     })
//     .state('app.home', {
//       url: '/',
//       templateUrl: 'partials/main.tpl.html',
//       controller: function() {},
//     })
//     .state('app.login', {
//       url: '/login',
//       templateUrl: 'partials/login.tpl.html',
//       controller: function() {},
//     })
//     .state('app.logout', {
//       url: '/logout',
//       templateUrl: 'partials/login.tpl.html',
//       controller: function() {},
//     })
//   ;
//   // for unmatch URL, redirect to /home
//   $urlRouterProvider.otherwise('/home');
// })
// .run(function($rootScope, $state) {
//   'ngInject';

//   $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
//     console.log('START TO LOAD:', toState);

//     if (!AuthService.getCurrentUser) {
//         $state.transitionTo("login");
//         event.preventDefault();
// //        return $state.go('app.login');
//     }
//   });

//   $rootScope.$on('$stateChangeSuccess', function(event, toState) {
//     console.log('LOADED:', toState);
//   });

//   $rootScope.$on('$stateChangeError', function(event, toState, b, c, e, error) {
//     console.log('FAILED TO LOAD:', toState, error);
//   });

//   $rootScope.$on('$stateNotFound', function(event, toState) {
//     console.log('FAILED NOT FOUND:', toState);
//   });
// })

;
