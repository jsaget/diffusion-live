var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('browser-js', function() {
  gulp.src([
      'node_modules/angular/angular.js',
      'node_modules/angular-sanitize/angular-sanitize.js',
      'node_modules/lodash/lodash.js',
      'node_modules/socket.io-client/socket.io.js',
      'client/browser/*.js'
    ])
    .pipe(concat('body.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('build/browser/js/'))
  ;
});

gulp.task('browser-cp', function() {
  gulp.src('client/browser/statics/**/*')
    .pipe(gulp.dest('build/browser/statics/'))
  ;
  gulp.src('client/browser/*.html')
    .pipe(gulp.dest('build/browser/'))
  ;
});

gulp.task('admin-js', function() {
  gulp.src([
      'node_modules/angular/angular.js',
      'node_modules/angular-sanitize/angular-sanitize.js',
      'node_modules/angular-drag-and-drop-lists/angular-drag-and-drop-lists.js',
      'node_modules/lodash/lodash.js',
      'node_modules/socket.io-client/socket.io.js',
      'client/admin/*.js',
    ])
    .pipe(concat('body.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('build/admin/js/'))
  ;
});

gulp.task('admin-cp', function() {
  gulp.src('client/admin/statics/**/*')
    .pipe(gulp.dest('build/admin/statics/'))
  ;
  gulp.src('client/admin/*.html')
    .pipe(gulp.dest('build/admin/'))
  ;
});


gulp.task('default', ['browser-js', 'browser-cp', 'admin-js', 'admin-cp'])
